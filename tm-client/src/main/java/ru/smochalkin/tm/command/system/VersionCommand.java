package ru.smochalkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractSystemCommand;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;

public final class VersionCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String arg() {
        return "-v";
    }

    @Override
    @NotNull
    public String name() {
        return "version";
    }

    @Override
    @NotNull
    public String description() {
        return "Display version.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        System.out.println("[VERSION]");
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
    }

}
