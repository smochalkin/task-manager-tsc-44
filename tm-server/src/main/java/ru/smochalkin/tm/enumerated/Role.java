package ru.smochalkin.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.exception.system.RoleNotFoundException;
import ru.smochalkin.tm.exception.system.StatusNotFoundException;

public enum Role {

    ADMIN("admin"), USER("user");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public static Role getRole(String value){
        try {
            return Role.valueOf(value);
        } catch (IllegalArgumentException e) {
            throw new RoleNotFoundException();
        }
    }

}
