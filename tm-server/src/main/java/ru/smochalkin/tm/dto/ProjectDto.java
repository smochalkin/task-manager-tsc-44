package ru.smochalkin.tm.dto;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class ProjectDto extends AbstractBusinessEntityDto {
}
