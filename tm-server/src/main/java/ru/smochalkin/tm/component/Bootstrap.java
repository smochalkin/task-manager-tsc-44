package ru.smochalkin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.endpoint.*;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.endpoint.*;
import ru.smochalkin.tm.service.*;
import ru.smochalkin.tm.service.dto.*;
import ru.smochalkin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;


@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectDtoService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskDtoService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskDtoService(connectionService);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    public final IUserService userService = new UserDtoService(propertyService, connectionService);

    @NotNull
    private final ISessionService sessionService = new SessionDtoService(connectionService, this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    private void init() {
        initPID();
        initEndpoints();
    }

    private void initEndpoints(){
        registryEndpoint(sessionEndpoint);
        registryEndpoint(adminEndpoint);
        registryEndpoint(userEndpoint);
        registryEndpoint(projectEndpoint);
        registryEndpoint(taskEndpoint);
    }

    private void registryEndpoint(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager-server.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(@Nullable final String[] args) {
        System.out.println("** WELCOME TO SERVER TASK MANAGER **");
        init();
    }

}
