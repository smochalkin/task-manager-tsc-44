package ru.smochalkin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.dto.IUserDtoRepository;
import ru.smochalkin.tm.dto.UserDto;

import javax.persistence.EntityManager;
import java.util.List;

public class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserDto e")
                .executeUpdate();
    }

    @NotNull
    public List<UserDto> findAll() {
        return entityManager.createQuery("SELECT e FROM UserDto e", UserDto.class).getResultList();
    }

    @Override
    @Nullable
    public UserDto findById(@Nullable final String id) {
        return entityManager.find(UserDto.class, id);
    }

    @Override
    @Nullable
    public UserDto findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("FROM UserDto e WHERE e.login = :login", UserDto.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    public void removeById(@Nullable final String id) {
        UserDto reference = entityManager.getReference(UserDto.class, id);
        entityManager.remove(reference);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM UserDto e WHERE e.login = :login")
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public int getCount() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM UserDto e", Long.class)
                .getSingleResult()
                .intValue();
    }

}

